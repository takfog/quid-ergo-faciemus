﻿using UnityEngine;
using System.Collections;

public class cambioscena : MonoBehaviour {

	public string nextScene;
	public GameObject container;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void cambioScena()
	{
		if (GameObject.FindGameObjectWithTag("RPC") == null)
			Application.LoadLevel(nextScene);
		else {
			container.SetActive(false);
			Destroy (Camera.main.gameObject);
			Application.LoadLevelAdditive (nextScene);
		}
	}
}
