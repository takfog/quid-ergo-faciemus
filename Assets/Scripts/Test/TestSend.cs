﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TestSend : MonoBehaviour {
	public InputField input;
	public Communicator comm;

	// Use this for initialization
	void Start () {
		comm = GameObject.FindGameObjectWithTag("RPC").GetComponent<Communicator> ();
	}
	
	public void Click() {
		comm.SendWrite(input.text);
	}
}
