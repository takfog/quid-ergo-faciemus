﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TestMove : MonoBehaviour {
	public GameObject cursor;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void GoToText(InputField txt) {
		cursor.GetComponent<CursorController>().Write(txt.text);
	}
	
	public void Force(InputField txt) {
		cursor.GetComponent<CursorController>().ForceWrite(txt.text);
	}

	public void ShowHide(GameObject txt) {
		txt.SetActive (!txt.activeSelf);
	}
}
