﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConnectionScript : MonoBehaviour {
	public InputField ip, port;
	public GameObject[] toDelete;
	public Text localIp;
	public string tabletLevel, gameLevel;

	void Start() {
		System.Net.IPAddress[] a =
			System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName());
		localIp.text = "Yout IP: " + a[0];
	}

	public void Play() {
		Debug.Log ("play on "+port.text);
		Network.InitializeServer(32, int.Parse(port.text), false);
	}

	public void Tablet() {
		Debug.Log ("tablet at "+ip.text+":"+port.text);
		Network.Connect(ip.text, int.Parse(port.text));
	}

	public void FreeTablet() {
		Debug.Log ("free tablet");
		LoadSection (tabletLevel);
	}

	void OnServerInitialized() {
		Debug.Log ("server init");
		LoadSection (gameLevel);
	}

	void OnConnectedToServer() {
		Debug.Log ("connected to server");
		LoadSection (tabletLevel);
	}

	void LoadSection(string name) {
		foreach (GameObject go in toDelete) {
			Destroy(go);
		}
		Application.LoadLevelAdditive(name);		         
	}


}
