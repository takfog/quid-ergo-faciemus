﻿using UnityEngine;
using System.Collections;

public class Communicator : MonoBehaviour {
	public CursorController cursor = null;

	// Use this for initialization
	void Start () {
	
	}
	
	public void SendWrite(string letter) {
		Debug.Log ("sending write");
		networkView.RPC ("Write", RPCMode.Others, letter);
	}

	[RPC]
	public void Write(string text) {
		Debug.Log ("received text " + text);
		if (cursor != null) {
			cursor.Write(text);
			Debug.Log("set");
		}
	}
	
	public void SendDestinationReached() {
		Debug.Log ("sending reach");
		networkView.RPC ("DestinationReached", RPCMode.Server);
	}
	
	[RPC]
	public void DestinationReached() {
		Debug.Log ("reached");
		QuestionManager manager = Camera.main.GetComponent<QuestionManager> ();
		if (manager != null)
			manager.AnswerToSelectedQuestion ();
	}

	
	public void SendForceWrite(string letter) {
		Debug.Log ("sending force write");
		networkView.RPC ("ForceWrite", RPCMode.Others, letter);
	}
	
	[RPC]
	public void ForceWrite(string text) {
		Debug.Log ("received forced text " + text);
		if (cursor != null) {
			cursor.ForceWrite(text);
			Debug.Log("set forced");
		}
	}

}
