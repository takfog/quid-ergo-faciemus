﻿using UnityEngine;
using System.Collections;

public class FadeIn : MonoBehaviour {
	public float transLen = 2;
	public float transTime = -1;
	public float maxAlpha = 1;
	public float minAlpha =0;
	public bool verso=true;
	bool stop=false;
	void Start() {
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		maxAlpha = sr.color.a;
		
		sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, minAlpha);
	}
	
	// Update is called once per frame
	void Update () {
		if (transTime >= 0) {
			transTime += Time.deltaTime;
			if (transTime > transLen) {
				transTime = -1;
				
			} else {
				float t = transTime/transLen;
				float v;
				
				SpriteRenderer sr = GetComponent<SpriteRenderer>();
				v = Mathf.Lerp(minAlpha, maxAlpha, t);
				sr.color = new Color(sr.color.r, sr.color.g,sr.color.b, v);
			}
		}
	}
	
	public void invertiVerso()
	{
		if (verso) {
			minAlpha=maxAlpha;
			maxAlpha = 0;
			
			verso = false;
		} 
		else {
			maxAlpha=minAlpha;
			minAlpha=0;
			verso = true;
		}
		transTime = 0;
		
	}
	
	public void StartTransition() {
		invertiVerso ();
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		sr.color = new Color(sr.color.r, sr.color.g,sr.color.b, minAlpha);
		if (transTime < 0) 
			transTime = 0;
		
	}
	
}
