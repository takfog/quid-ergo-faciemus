﻿using UnityEngine;
using System.Collections;

public class Zoom : MonoBehaviour {
	public float transLen = 2;
	public float transTime = -1;

	public float minZoom, maxZoom, minRot, maxRot;
	public Vector3 minPos, maxPos;
	public bool isSmall = true;

	void Start() {
		if (isSmall) {
			if (isSmall) {
				minZoom = transform.localScale.x;
				minRot = transform.eulerAngles.z;
				minPos = transform.position;
			} else {
				maxZoom = transform.localScale.x;
				maxRot = transform.eulerAngles.z;
				maxPos = transform.position;
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (transTime >= 0) {
			transTime += Time.deltaTime;
			if (transTime > transLen) {
				transTime = -1;
				isSmall = !isSmall;
				if (isSmall) {
					transform.localScale = new Vector3(minZoom, minZoom, minZoom);
					transform.eulerAngles = new Vector3(0, 0, minRot);
					transform.position = minPos;
				} else {
					transform.localScale = new Vector3(maxZoom, maxZoom, maxZoom);
					transform.eulerAngles = new Vector3(0, 0, maxRot);
					transform.position = maxPos;
				}
			} else {
				float t = transTime/transLen;
				float v;

				v = Mathf.Lerp(transform.localScale.x, isSmall ? maxZoom : minZoom, t);
				transform.localScale = new Vector3(v, v, v);

				v = Mathf.LerpAngle(transform.eulerAngles.z, isSmall ? maxRot : minRot, t);
				transform.eulerAngles = new Vector3(0, 0, v);

	//			SpriteRenderer sr = GetComponent<SpriteRenderer>();
	//			v = Mathf.Lerp(sr.color.a, isSmall ? 1 : 0, t);
	//			sr.color = new Color(sr.color.r, sr.color.g,sr.color.b, v);

				Vector3 newPos = transform.position;
				newPos.x = Mathf.Lerp(transform.position.x, isSmall ? maxPos.x : minPos.x, t);
				newPos.y = Mathf.Lerp(transform.position.y, isSmall ? maxPos.y : minPos.y, t);
				newPos.z = Mathf.Lerp(transform.position.z, isSmall ? maxPos.z : minPos.z, t);
				transform.position = newPos;
			}
		}
	}

	public void StartTransition() {
		if (transTime < 0) 
			transTime = 0;
		else {
			isSmall = !isSmall;
			transTime = transLen - transTime;
		}
	}

}
