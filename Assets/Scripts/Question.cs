﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Question  {
	public int key;
	public string question;
	public string answer;
	public bool used=false;
	public bool active;
	public List<int> fathers;

	public Question(int key, string question,string answer,bool active)
	{
		this.key=key;
		this.question=question;
		this.answer=answer;
		this.active = active;
		fathers = new List<int>();
	}


	public bool CanIDrawIt()
	{
		return !used&&checkFathers();
	}

	public void addDependencies(int key)
	{
		fathers.Add (key);
	}

	public bool checkFathers()
	{
		if (active)
			return true;
		if (Camera.main.GetComponent<QuestionManager> ().checkFathers (fathers)) {
						active = true;
			            return true;
				}
		return false;
	}
	
	public void AnswerToQuestion()
	{
		used=true;
	}

	public string GetFathersToString()
	{
		string s = "";
		foreach (int key in fathers)
			s += key+";";
		return s;
	}
}
