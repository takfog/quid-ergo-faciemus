﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneQuestionManager : MonoBehaviour {
	public int[] QuestionsKey;
	private QuestionManager QM;
	// Use this for initialization
	void Start () {
		QM = Camera.main.GetComponent<QuestionManager> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void drawButtons()
	{
		Camera.main.GetComponent<QuestionManager> ().DisableAllButtons();
		int b = 0;
		for (int i=0; i<QuestionsKey.Length; i++)
			if (QM.CanIDrawIt (QuestionsKey [i])) 
		        {
			       QM.ActiveButton(b,QuestionsKey [i]);
			       b++;
		        }
	}
}
