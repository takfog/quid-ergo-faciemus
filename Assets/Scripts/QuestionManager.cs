﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Text;
using System;
using System.Collections.Generic;

public class QuestionManager : MonoBehaviour {
	public GameObject[] buttons;
	public Button backButton;
	public Text nameLabel, descLabel;

	public List<Question> questions;
	private Question[] onScreen;
	private Item attivo;
	public GameObject canvace;
	public Communicator communicator = null;
	public Question selectedQuestion;

	public GameObject ghost;
	public int finalQuestion;

	// Use this for initialization
	void Start () {
		canvace.SetActive (false);
		ghost.SetActive (false);

		GameObject commHolder = GameObject.FindGameObjectWithTag ("RPC");
		if (commHolder != null)
			communicator = commHolder.GetComponent<Communicator> ();
		onScreen=new Question[5];
		//buttons=GameObject.FindGameObjectsWithTag ("Question");
		DisableAllButtons ();
		questions= new List<Question>();
		Debug.Log ("about to load");
		Load("Questions.txt");

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void clicked(int buttonKey)
	{
		selectedQuestion = onScreen [buttonKey];
		EnableButtons (false);
		if (communicator != null)
			communicator.SendWrite(selectedQuestion.answer);
		else {
			Debug.Log("answer: "+selectedQuestion.answer);
			AnswerToSelectedQuestion();
		}
	}

	void EnableButtons(bool enabled) {
		for(int i=0;i<buttons.Length;i++)
			buttons[i].GetComponent<Button> ().interactable = enabled;
		backButton.interactable = enabled;
	}

	public void AnswerToSelectedQuestion() {
		if (ghost.activeSelf) {
			GetComponent<GhostManager>().ShowQuestion();
		} else {
			selectedQuestion.AnswerToQuestion ();
			ReActiveButtons ();
			if (CanIDrawIt (finalQuestion)) {
				exit ();
				GetComponent<GhostManager>().ShowGhost();
			}
		}
	}

	public void ReActiveButtons ()
	{
		EnableButtons (true);
		drawButtons (attivo);
	}

	public bool checkFathers(List<int> fathers)
	{
		foreach (int key in fathers) 
		{
			if(!questions[key].used)
				return false;
		}
		return true;
		}

	public bool checkUsed(int key)
	{
		return questions [key].used;
	}

	public void ActiveButton(int index,int keyQ)
	{
		buttons [index].SetActive (true);
		buttons [index].GetComponent<Button>().transform.FindChild("Text").GetComponent<Text>().text=questions [keyQ].question;
	}

	public void DisableAllButtons()
	{
		for (int i=0; i<buttons.Length; i++)
			DisableButton (i);
	}

	public void DisableButton(int index)
	{
		buttons [index].SetActive (false);
		buttons [index].GetComponent<Button>().transform.FindChild("Text").GetComponent<Text>().text="";
		onScreen [index] = null;
	}

	public Boolean CanIDrawIt(int key)
	{
		if(key<questions.Count)
		    return questions[key].CanIDrawIt ();
		return false;
	}

	public void exit()
	{
		DisableAllButtons();
		canvace.SetActive (false);
		attivo.GetComponent<Zoom> ().StartTransition ();
		attivo = null;
	}

	public bool HasActiveItem() {
		return attivo != null || ghost.activeSelf;
	}

	public void drawItem(Item item)
	{
		if (attivo != null)
			return;
		attivo = item;
		nameLabel.text = attivo.itemName;
		descLabel.text = attivo.description;
		drawButtons (item);
		canvace.SetActive (true);
		}

	public void drawButtons(Item item)
	{

		DisableAllButtons();
		int b = 0;

		for (int i=0; i<item.QuestionsKey.Length; i++) {
			if (CanIDrawIt (item.QuestionsKey [i])) {
				ActiveButton (b, item.QuestionsKey [i]);
				onScreen[b]=questions[item.QuestionsKey [i]];
						b++;

						}
				}
	}
	
	
	private bool Load(string fileName)
	{

		// Handle any problems that might arise when reading the text
		try
		{
			string line;
			// Create a new StreamReader, tell it which file to read and what encoding the file
			// was saved as
			Debug.Log(fileName);
			StreamReader theReader = new StreamReader(fileName, Encoding.Default);
			Debug.Log(theReader);

			using (theReader)
			{
				// While there's lines left in the text file, do this:
				do
				{
					line = theReader.ReadLine();
					if (line != null)
					{
						string[] entries = line.Split(';');

						if (entries.Length > 0)
						{
							if(entries.Length>3){
								Question q=new Question(Int32.Parse(entries[0]),entries[1],entries[2],false);
							for(int i=3;i<entries.Length;i++)
						        q.addDependencies(Int32.Parse(entries[i]));
								questions.Add(q);
							}
						else 
								questions.Add(new Question(Int32.Parse(entries[0]),entries[1],entries[2],true));

								
					}
				}
				}
				while (line != null);
				Debug.Log(questions.Count);
				// Done reading, close the reader and return true to broadcast success
				theReader.Close();
				return true;
			}
		}
		
		// If anything broke in the try block, we throw an exception with information
		// on what didn't work
		catch (Exception e)
		{
			return false;
		}
	}

}
