﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HowToShow : MonoBehaviour {
	public GameObject cursor;
	public GameObject hand;
	public GameObject container;

	public AudioClip touch, follow, cantMove;
	public AudioSource speech;

	public GameObject[] steps;
	public GameObject lastStep;
	public Vector3 handTouch = new Vector3(0.4f, -2.5f, 0), handNotTouch = new Vector3(1, -3.5f, 0);

	public Text description;

	public float speed = 0.5f;
	public float waitTime = 2;

	public int phase = 0;
	public float time = 0;
	public bool newStep = true;

	Vector3 handStart, cursorStart;


	// Use this for initialization
	void Start () {
		handStart = hand.transform.localPosition;
		cursorStart = cursor.transform.position;
	}

	public void ExitTutorial() {
		string level = "game";
		if (GameObject.FindGameObjectWithTag("RPC") == null)
			Application.LoadLevel(level);
		else {
			container.SetActive(false);
			Destroy (Camera.main.gameObject);
			Application.LoadLevelAdditive (level);
		}
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		bool finished = false;
		if (phase == 0)
			finished = TouchCursor(true);
		else if (phase == 1)
			finished = Wait(waitTime);
		else if (phase-2 < steps.Length) //2 -> steps.length+1
			finished = FollowCursor(phase-2);
		else {
			switch(phase-2 - steps.Length) {
			case 0:
				finished = StopTouch();
				break;
			case 1:
				finished = Wait(waitTime);
				break;
			case 2:
				finished = TouchCursor(false);
				break;
			case 3:
				finished = LastStep();
				break;
			}
		}

		if (finished) {
			phase++;
			time = 0;
			newStep = true;
		}
	}

	Vector3 NewPosition(Vector3 start, Vector3 end, float scale, out bool finished) {
		float t = time * speed / (scale * (start - end).magnitude);
		finished = t > 1;
		if (finished) {
			return new Vector3(end.x, end.y, start.z);
		} else {

			finished = false;
			float x = Mathf.Lerp (start.x, end.x, t);
			float y = Mathf.Lerp (start.y, end.y, t);
			return new Vector3(x, y, start.z);
		}

	}

	bool TouchCursor(bool firstCall) {
		if (newStep) {
			//init
			if (firstCall) {
				description.text = "Touch the planchette with one finger.";
				speech.PlayOneShot(touch);
			}
			newStep = false;
		}
		bool finished;
		hand.transform.localPosition = NewPosition(firstCall ? handStart : handNotTouch, handTouch, hand.transform.localScale.x, out finished);
		return finished;
	}

	bool Wait(float waitTime) {
		return time >= waitTime;
	}

	bool FollowCursor(int step) {
		if (newStep && step == 0) {
			//init
			description.text = "Keep touching and follow the planchette to see the letters written by the spirit.";
			speech.PlayOneShot(follow);
			newStep = false;
		}
		Vector3 start = (step == 0) ? cursorStart : steps[step-1].transform.position;
		bool finished;
		cursor.transform.position = NewPosition(start, steps[step].transform.position, 1, out finished);
		return finished;
	}

	bool StopTouch() {
		if (newStep) {
			//init
			description.text = "Keep touching and follow the planchette to see the letters written by the spirit.\nThe spirit can't move the planchette if you don't touch it.";
			speech.PlayOneShot(cantMove);
			newStep = false;
		}
		bool finished;
		hand.transform.localPosition = NewPosition(handTouch, handNotTouch, hand.transform.localScale.x, out finished);
		return finished;
	}

	bool LastStep() {
		bool finished;
		cursor.transform.position = NewPosition(steps[steps.Length-1].transform.position, lastStep.transform.position, 1, out finished);
		return finished;
	}
}
