﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour {
	public GameObject activeScene;
	// Use this for initialization
	void Start () {

		GameObject[] scene=GameObject.FindGameObjectsWithTag ("Scene");
		for(int i=0;i<scene.Length;i++)
			scene[i].SetActive(false);
		activeScene.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeScene(GameObject scene)
	{
		activeScene.SetActive (false);
		activeScene = scene;
		activeScene.SetActive (true);

	}
}
