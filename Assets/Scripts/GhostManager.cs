﻿using UnityEngine;
using System.Collections;

public class GhostManager : MonoBehaviour {
	public GameObject ghost;
	public AudioClip ghostAppears, winClip, loseClip;
	public GameObject canvas, loseCanvas, winCanvas;
	public Communicator communicator = null;

	void Start() {
		canvas.SetActive (false);
		loseCanvas.SetActive (false);
		winCanvas.SetActive (false);
		GameObject commHolder = GameObject.FindGameObjectWithTag ("RPC");
		if (commHolder != null)
			communicator = commHolder.GetComponent<Communicator> ();
	}

	public void ShowGhost() {
		//fase finale, appare il fantasma
		AudioSource src = Camera.main.GetComponent<AudioSource> ();
		src.clip = ghostAppears;
		src.Play ();
		ghost.SetActive (true);
		ghost.GetComponent<FadeIn> ().StartTransition ();
		if (communicator != null) {
			communicator.SendForceWrite("whoami");
		} else {
			Debug.Log("whoami");
			ShowQuestion();
		}
	}

	public void ShowQuestion() {
		canvas.SetActive (true);
		ghost.GetComponent<AudioSource>().Play();
	}


	public void Correct() {
		AudioSource src = Camera.main.GetComponent<AudioSource> ();
		src.clip = winClip;
		src.Play ();
		canvas.SetActive (false);
		winCanvas.SetActive (true);
		ghost.SetActive (false);
	}

	public void Wrong() {
		AudioSource src = Camera.main.GetComponent<AudioSource> ();
		src.clip = loseClip;
		src.Play ();
		canvas.SetActive (false);
		loseCanvas.SetActive (true);
	}
}
