﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {
	public int[] QuestionsKey;
	public string itemName, description;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseUpAsButton()
	{
		QuestionManager man = Camera.main.GetComponent<QuestionManager> ();
		if (!man.HasActiveItem()) {
			GetComponent<Zoom> ().StartTransition ();
			man.drawItem(this);
		}
	}
}
