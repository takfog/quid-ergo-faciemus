﻿using UnityEngine;
using System.Collections;

public class CursorController : MonoBehaviour {
	public float accuracy = 0.05f;

	public float movement = 0.05f;
	public float forceMovement = 0.5f;
	public float totPause = 0.6f;
	public float totForcedPause = 0.6f;

	public bool isMoving = false;
	public bool hasMid = false;
	public bool isTouching = false;
	public bool forced = false;

	public Vector3 midDest;
	public Vector3 destination;

	public float pause = 0;

	public Vector3 lastPos;

	public string toWrite = null;
	public int writeIdx = 0;

	Vector3 basePos;
	public GameObject yes, no, goodbye;
	public GameObject[] letters, numbers;

	public bool vibration = false;

	Communicator communicator = null;
	AudioSource sound;

	// Use this for initialization
	void Start () {
		basePos = transform.position;
		sound = GetComponent<AudioSource>();
		GameObject com = GameObject.FindGameObjectWithTag ("RPC");
		if (com != null) {
			communicator = com.GetComponent<Communicator> ();
			communicator.cursor = GetComponent<CursorController>();
		}
	}
	
	// Update is called once per frame
	void Update () {
		bool moved = false;
		if (pause > 0)
			pause -= Time.deltaTime;
		if (isMoving && pause <= 0 && (Input.GetMouseButton(0) || forced)) {
			Vector3 click = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			click.z = collider2D.bounds.center.z;
			if (forced || collider2D.bounds.Contains(click)) {
				click.z = transform.position.z;
				Vector3 actDest = hasMid ? midDest : destination;

				Vector3 userDir;
				if (isTouching) {
					userDir = click - lastPos;
				} else {
					isTouching = true;
					vibration = false;
					userDir = new Vector3();
				}

				Vector3 ghostDir = actDest - transform.position;
				float movementToUse = forced ? forceMovement : movement;
				if (!forced || ghostDir.magnitude > movementToUse) {
					ghostDir.Normalize();
					ghostDir = ghostDir * movementToUse;
				}

				Vector3 move;
				if (forced) {
					move = ghostDir;
				} else {
					float concordance = (Vector3.Dot(ghostDir, userDir)+1)/2;
					//Debug.Log(concordance);
					if (concordance > 0.50001f) {
						vibration = false;
					} else if (concordance < 0.5f && !vibration) {
	//					Handheld.Vibrate();
						vibration = true;
					}

					move = ghostDir*concordance + userDir*(1-concordance);
				}
				move.z = 0;
				Vector3 newDest = transform.position + move;

				if ((newDest - actDest).magnitude < accuracy) {
					//movimento finito
					transform.position = actDest;
					EndMovement();
					lastPos = click;
				} else {
					//muovi ancora
					transform.position = newDest;
					lastPos = click; //+= move;
					moved = true;
				}


			} else {
				isTouching = false;
			}
		} else {
			isTouching = false;
		}
		sound.enabled = moved;
	}

	void EndMovement() {
		if (hasMid) {
			pause = 0; //tappa intermedia, niente pausa
			hasMid = false;
		} else {
			isMoving = false;
			pause = (forced) ? totForcedPause : totPause; //tappa finale, pausa

			if (toWrite != null) {
				writeIdx++;
				if (writeIdx < toWrite.Length) {
					SetDestination(toWrite[writeIdx]);
				} else {
					toWrite = null;
					if (communicator != null)
						communicator.SendDestinationReached();
				}
			}
		}
	}
	
	public void SetDestination(Vector3 destination) {
		this.destination = destination;
		this.destination.z = transform.position.z;
		if ((transform.position - this.destination).magnitude < 0.1f) {
			midDest = destination;
			midDest.y += 0.4f;
			hasMid = true;
		} else {
			hasMid = false;
		}
		isMoving = true;
	}
	
	public void SetDestination(char destination) {
		if (destination == '!') //yes
			SetDestination(yes.transform.position);
		else if (destination == '-') //no
			SetDestination(no.transform.position);
		else if (destination == '^') //base
			SetDestination(basePos);
		else if (destination == '$') //goodbye
			SetDestination(goodbye.transform.position);
		else if (char.IsDigit (destination))
			SetDestination(numbers [destination - '0'].transform.position);
		else
			SetDestination(letters [destination - 'a'].transform.position);
	}

	public void Write(string text) {
		toWrite = text;
		writeIdx = 0;

		forced = false;
		SetDestination(text[0]);
	}

	public void ForceWrite(string text) {
		Write (text);
		forced = true;
	}

}
